# Contributor: kohnish <kohnish@gmx.com>
# Contributor: Michał Polański <michal@polanski.me>
# Maintainer: Michał Polański <michal@polanski.me>
pkgname=buildah
pkgver=1.31.3
pkgrel=1
pkgdesc="tool that facilitates building OCI container images"
url="https://github.com/containers/buildah"
license="Apache-2.0"
arch="all"
depends="oci-runtime shadow-subids slirp4netns containers-common"
makedepends="go go-md2man lvm2-dev gpgme-dev libseccomp-dev btrfs-progs-dev bash"
subpackages="$pkgname-doc"
options="!check" # tests require root privileges
source="https://github.com/containers/buildah/archive/v$pkgver/buildah-$pkgver.tar.gz"

# secfixes:
#   1.28.0-r0:
#     - CVE-2022-2990
#   1.21.3-r0:
#     - CVE-2021-3602
#   1.19.4-r0:
#     - CVE-2021-20206
#   1.14.4-r0:
#     - CVE-2020-10696

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	GIT_COMMIT="$pkgver" make
}

package() {
	GIT_COMMIT="$pkgver" make install PREFIX=/usr DESTDIR="$pkgdir"
}

sha512sums="
efa656c818f7b07f903cb5f70b2c50c8d5b85a11ce9093707d051f3d6e67a8dfa5ab6ecb720900c563d879173ebeb3ac0e5175c0090f4608a0dd253f4149af93  buildah-1.31.3.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kalarm
pkgver=23.08.0
pkgrel=0
pkgdesc="Personal alarm scheduler"
url="https://kontact.kde.org/"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="kdepim-runtime"
makedepends="
	akonadi-contacts-dev
	akonadi-dev
	akonadi-mime-dev
	extra-cmake-modules
	kauth-dev
	kcalendarcore-dev
	kcalutils-dev
	kcmutils-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdelibs4support-dev
	kdoctools-dev
	kglobalaccel-dev
	kguiaddons-dev
	kholidays-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kidletime-dev
	kimap-dev
	kio-dev
	kjobwidgets-dev
	kmailtransport-dev
	kmime-dev
	knotifications-dev
	knotifyconfig-dev
	kpimtextedit-dev
	kservice-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	kxmlgui-dev
	libkdepim-dev
	libxslt-dev
	mailcommon-dev
	phonon-dev
	pimcommon-dev
	qgpgme
	qt5-qtbase-dev
	qt5-qtx11extras-dev
	samurai
	"
_repo_url="https://invent.kde.org/pim/kalarm.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kalarm-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Tests are broken

build() {
	cmake -B build -G Ninja\
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
b682425e69090e100495bb8d283b12fd11306e21180eb8999339d59caf23455a38feca40f28a20d9a27e6f1358a20dbfec525f70f806e90501575b0a2d6de837  kalarm-23.08.0.tar.xz
"

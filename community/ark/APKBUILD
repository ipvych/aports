# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=ark
pkgver=23.08.0
pkgrel=0
pkgdesc="Graphical file compression/decompression utility with support for multiple formats"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.ark"
license="GPL-2.0-only"
depends="
	7zip-virtual
	lrzip
	unzip
	zip
	zstd
	"
makedepends="
	extra-cmake-modules
	karchive-dev
	kconfig-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemmodels-dev
	kparts-dev
	kpty-dev
	kservice-dev
	kwidgetsaddons-dev
	libarchive-dev
	libzip-dev
	qt5-qtbase-dev
	samurai
	shared-mime-info
	xz-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/utilities/ark.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/ark-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

# secfixes:
#   20.08.0-r1:
#     - CVE-2020-24654
#   20.04.3-r1:
#     - CVE-2020-16116

case "$CARCH" in
	# FIXME: tests fail, see alpine/aports#14266
	x86_64) options="!check";;
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run -a ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
84fc163e2f5c1b41d72978bb0f8a3e12aab68d681c7b2224dbf3b37bffd3c61814f54d1f647ec3aec34a2024275e7258f320fa2e692f2de08108999a2140d578  ark-23.08.0.tar.xz
"

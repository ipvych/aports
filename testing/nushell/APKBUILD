# Contributor: nibon7 <nibon7@163.com>
# Maintainer: nibon7 <nibon7@163.com>
pkgname=nushell
pkgver=0.84.0
pkgrel=0
pkgdesc="A new type of shell"
url="https://www.nushell.sh"
# s390x: nix crate
arch="all !s390x"
license="MIT"
makedepends="
	cargo
	cargo-auditable
	libgit2-dev
	openssl-dev
	sqlite-dev
	"
checkdepends="bash"
subpackages="$pkgname-plugins:_plugins"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.pre-deinstall"
source="$pkgname-$pkgver.tar.gz::https://github.com/nushell/nushell/archive/$pkgver.tar.gz
	system-deps.patch
	"

case "$CARCH" in
# ooms when building
armhf|armv7|ppc64le|riscv64|x86) _exclude_opts="--exclude nu-cmd-dataframe" ;;
esac

prepare() {
	default_prepare

	# Rust target triple.
	local target=$(rustc -vV | sed -n 's/host: //p')

	# Build against system-provided libs
	mkdir -p .cargo
	cat >> .cargo/config.toml <<-EOF
		[target.$target]
		git2 = { rustc-link-lib = ["git2"] }
		rusqlite = { rustc-link-lib = ["sqlite3"] }
	EOF

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --workspace --release --frozen $_exclude_opts
}

check() {
	cargo test --workspace --frozen $_exclude_opts
}

package() {
	find target/release \
		-maxdepth 1 \
		-executable \
		-type f \
		-name "nu*" \
		-exec install -Dm755 '{}' -t "$pkgdir"/usr/bin/ \;
}

_plugins() {
	pkgdesc="Nushell plugins"
	depends="nushell=$pkgver-r$pkgrel"

	amove usr/bin/nu_plugin_*
}

sha512sums="
d3bd4b312a86a604e738cf7e0da8931de255858e0f2f430e28afa7af93203845c8e559bf944b966d6f18e837b6a589cc3d0372a58425940432e59096ed564e78  nushell-0.84.0.tar.gz
18f778c6118305b9accb592d017f0a5c085f9abfcbec57388b9b8c2787ec1b00a3c4155c7979ebcb4800a63fa7eafc45943fa3ea7a727788de789bbe9294f042  system-deps.patch
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=pimcommon
pkgver=23.08.0
pkgrel=0
pkgdesc="Common lib for KDEPim"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url='https://community.kde.org/KDE_PIM'
license="GPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	karchive-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcontacts-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	kimap-dev
	kio-dev
	kitemmodels-dev
	kjobwidgets-dev
	kmime-dev
	knewstuff-dev
	kpimtextedit-dev
	kservice-dev
	ktextaddons-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdepim-dev
	purpose-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/pimcommon.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/pimcommon-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="net" # net required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
db2951f456c8ff3a48406f8c43828bb9661af0bd40f2f086776ca7ec24710763452ee32534860e325d776400d01740b1f27b5b9e71166cf55eda9725075c0b5b  pimcommon-23.08.0.tar.xz
"

From fd0a65cc19e843253eb91102a7c9caa59fd16b1f Mon Sep 17 00:00:00 2001
From: Coco Liliace <coco@liliace.dev>
Date: Wed, 6 Sep 2023 14:16:30 +0800
Subject: [PATCH] fix zig-0.11 breaking change

---
 build.zig    | 27 ++++++++++++++++-----------
 src/args.zig |  6 +++---
 src/main.zig | 14 +++++++-------
 3 files changed, 26 insertions(+), 21 deletions(-)

diff --git a/build.zig b/build.zig
index 2e77a66..fa8d618 100644
--- a/build.zig
+++ b/build.zig
@@ -1,7 +1,7 @@
 const std = @import("std");
 const gitVersionTag = @import("src/gitversiontag.zig").gitVersionTag;
 
-pub fn build(b: *std.build.Builder) void {
+pub fn build(b: *std.Build) void {
     // write src/version.zig
     const alloc = std.heap.page_allocator;
     const gvs = gitVersionTag(alloc);
@@ -28,15 +28,18 @@ pub fn build(b: *std.build.Builder) void {
 
     // Standard release options allow the person running `zig build` to select
     // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
-    const mode = b.standardReleaseOptions();
-
-    const exe = b.addExecutable("aercbook", "src/main.zig");
-    exe.setTarget(target);
-    exe.setBuildMode(mode);
+    const optimize = b.standardOptimizeOption(.{});
+
+    const exe = b.addExecutable(.{
+        .name = "aercbook",
+        .root_source_file = .{ .path = "src/main.zig" },
+        .target = target,
+        .optimize = optimize,
+    });
     exe.pie = true;
-    exe.install();
+    b.installArtifact(exe);
 
-    const run_cmd = exe.run();
+    const run_cmd = b.addRunArtifact(exe);
     run_cmd.step.dependOn(b.getInstallStep());
     if (b.args) |args| {
         run_cmd.addArgs(args);
@@ -45,9 +48,11 @@ pub fn build(b: *std.build.Builder) void {
     const run_step = b.step("run", "Run the app");
     run_step.dependOn(&run_cmd.step);
 
-    const exe_tests = b.addTest("src/main.zig");
-    exe_tests.setTarget(target);
-    exe_tests.setBuildMode(mode);
+    const exe_tests = b.addTest(.{
+        .root_source_file = .{ .path = "src/main.zig" },
+        .target = target,
+        .optimize = optimize,
+    });
 
     const test_step = b.step("test", "Run unit tests");
     test_step.dependOn(&exe_tests.step);
diff --git a/src/args.zig b/src/args.zig
index daf27ac..a243c98 100644
--- a/src/args.zig
+++ b/src/args.zig
@@ -172,7 +172,7 @@ fn parseInternal(comptime Generic: type, comptime MaybeVerb: ?type, args_iterato
                 try arglist.append(try result_arena_allocator.dupeZ(u8, item));
             } else {
                 var any_shorthands = false;
-                for (item[1..]) |char, index| {
+                for (item[1..], 0..) |char, index| {
                     var option_name = [2]u8{ '-', char };
                     var found = false;
                     if (@hasDecl(Generic, "shorthands")) {
@@ -288,7 +288,7 @@ fn parseInternal(comptime Generic: type, comptime MaybeVerb: ?type, args_iterato
         try arglist.append(try result_arena_allocator.dupeZ(u8, item));
     }
 
-    result.positionals = arglist.toOwnedSlice();
+    result.positionals = try arglist.toOwnedSlice();
     return result;
 }
 
@@ -416,7 +416,7 @@ fn parseInt(comptime T: type, str: []const u8) !T {
                 if (comptime std.math.maxInt(T) < 1024)
                     return error.Overflow;
                 var base: T = if (base1024) 1024 else 1000;
-                multiplier = try std.math.powi(T, base, @intCast(T, pow));
+                multiplier = try std.math.powi(T, base, @intCast(pow));
             }
         }
     }
diff --git a/src/main.zig b/src/main.zig
index ae21302..2f47216 100644
--- a/src/main.zig
+++ b/src/main.zig
@@ -1,17 +1,17 @@
 const std = @import("std");
 const edit_distance = @import("levenshtein.zig").edit_distance;
-const sort = std.sort.sort;
+const sort = std.sort.heap;
 const version_string = @import("version.zig").version_string;
 const argsParser = @import("args.zig");
 
 var input: []const u8 = undefined;
 
 fn score(input_word: []const u8, compared_to: []const u8) i32 {
-    var dist: i32 = @intCast(i32, edit_distance(input_word, compared_to));
+    var dist: i32 = @intCast(edit_distance(input_word, compared_to));
     if (std.mem.startsWith(u8, compared_to, input_word)) {
         // if the input matches beginning of compared_to, we decrease the
         // distance to rank it higher at the top
-        dist -= @intCast(i32, input_word.len * 2);
+        dist -= @intCast(input_word.len * 2);
     }
     return dist;
 }
@@ -243,19 +243,19 @@ fn parseMailFromStdin(alloc: std.mem.Allocator) !ParseMailResult {
         }
 
         if (std.ascii.eqlIgnoreCase(line[0..5], "from:")) {
-            from_pos = it.index.? - line.len + @intCast(usize, 4);
+            from_pos = it.index.? - line.len + 4;
             current_end = &from_end;
             from_end = it.index.? - 2;
             continue;
         }
         if (std.ascii.eqlIgnoreCase(line[0..3], "to:")) {
-            to_pos = it.index.? - line.len + @intCast(usize, 2);
+            to_pos = it.index.? - line.len + 2;
             current_end = &to_end;
             to_end = it.index.? - 2;
             continue;
         }
         if (std.ascii.eqlIgnoreCase(line[0..3], "cc:")) {
-            cc_pos = it.index.? - line.len + @intCast(usize, 2);
+            cc_pos = it.index.? - line.len + 2;
             current_end = &cc_end;
             cc_end = it.index.? - 2;
             continue;
@@ -463,7 +463,7 @@ pub fn main() anyerror!void {
 
         // default: levenshtein search
         sort([]const u8, list.items, {}, comptime comp_levenshtein([]const u8));
-        for (list.items[0..std.math.min(5, list.items.len)]) |key| {
+        for (list.items[0..@min(5, list.items.len)]) |key| {
             if (map.get(key)) |v| {
                 // bug in aerc.conf: tab separated lines are NOT supported
                 // std.debug.print("{s}\t{s}\n", .{ v, key });
-- 
2.42.0


# Contributor: Guy Godfroy <guy.godfroy@gugod.fr>
# Maintainer: Guy Godfroy <guy.godfroy@gugod.fr>
pkgname=py3status
pkgver=3.52
pkgrel=0
pkgdesc="Extensible i3status wrapper written in python"
url="https://py3status.readthedocs.io"
arch="noarch"
license="BSD-3-Clause"
depends="python3"
makedepends="
	py3-gpep517
	py3-installer
	py3-wheel
	py3-hatchling
	py3-setuptools
	"
checkdepends="py3-pytest"
subpackages="$pkgname-doc $pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/ultrabug/py3status/archive/refs/tags/$pkgver.tar.gz"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
	install -d "$pkgdir/usr/share/doc/$pkgname"
	install -d "$pkgdir/usr/share/doc/$pkgname/dev-guide"
	install -d "$pkgdir/usr/share/doc/$pkgname/user-guide"
	install -m644 docs/*.md README.md CHANGELOG "$pkgdir/usr/share/doc/$pkgname"
	install -m644 docs/dev-guide/* "$pkgdir/usr/share/doc/$pkgname/dev-guide"
	install -m644 docs/user-guide/* "$pkgdir/usr/share/doc/$pkgname/user-guide"
	install -Dm644 LICENSE "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}

sha512sums="
26c4b78188aef794a92d36fc6906d4c0b8ac397d35a283d129bafc0a131279b5944d85fc453b52e0221d329a61af9f411e5b155d4bac167e8430759ed9501559  py3status-3.52.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kajongg
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="noarch !armhf"
url="https://kde.org/applications/games/org.kde.kajongg"
pkgdesc="Mah Jongg - the ancient Chinese board game for 4 players"
license="GPL-2.0-or-later AND GFDL-1.2-only"
depends="
	python3
	py3-twisted
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kconfigwidgets-dev
	kdoctools-dev
	libkmahjongg-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/kajongg.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kajongg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang $pkgname-pyc"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

pyc() {
	default_pyc
	amove usr/share/kajongg/__pycache__
}

sha512sums="
4bf5fd99bcd8312f2e27d2a7699e166f53809d9d3d1bb11a9ff976f480c137e1a6da2ff1739e39977ef6acf05e82b5a8b61e6a24fb172fd80fd4038c9097f4a8  kajongg-23.08.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=arianna
pkgver=23.08.0
pkgrel=1
pkgdesc="EPub Reader for mobile devices"
url="https://invent.kde.org/graphics/arianna/"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
license="(BSD-2-Clause OR BSD-3-Clause) AND (GPL-2.0-only OR GPL-3.0-only) AND (LGPL-2.0-or-later OR LGPL-2.1-or-later OR LGPL-3.0-or-later)"
depends="
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	baloo-dev
	karchive-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	kfilemetadata-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami2-dev
	kwindowsystem-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qtwebengine-dev
	qt5-qtwebsockets-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/graphics/arianna.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/arianna-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cdec20a520265c9b9934ce94d7397c47b8af1e52bb7ec5b7f7aeee54687d9fec02c518239d77c619cf4beffe31e0a83f8daa0ddd15621bf45fe84ce2a96083d3  arianna-23.08.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kompare
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/development/org.kde.kompare"
pkgdesc="Graphical File Differences Tool"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcodecs-dev
	kconfig-dev
	kcoreaddons-dev
	kdoctools-dev
	kiconthemes-dev
	kjobwidgets-dev
	kparts-dev
	ktexteditor-dev
	kwidgetsaddons-dev
	libkomparediff2-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/sdk/kompare.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kompare-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6ba43c73eec52b87b11a960527e88b01bc045274f389d6940f3a48ec8b944e0fb07c534c7787384786f31be97e12addaf87a9ff247fe58064ab8b9dbfe9477f0  kompare-23.08.0.tar.xz
"

# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Simon Zeni <simon@bl4ckb0ne.ca>
pkgname=glslang
pkgver=1.3.261.0
pkgrel=0
pkgdesc="Khronos reference front-end for GLSL, ESSL, and sample SPIR-V generator"
url="https://github.com/KhronosGroup/glslang"
arch="all"
license="BSD-3-Clause"
depends_dev="$pkgname"
makedepends="cmake samurai python3 bison spirv-tools-dev"
checkdepends="bash gtest-dev"
subpackages="$pkgname-static $pkgname-libs $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/KhronosGroup/glslang/archive/refs/tags/sdk-$pkgver.tar.gz"
builddir="$srcdir/glslang-sdk-$pkgver"

case "$CARCH" in
s390x) options="$options !check" ;; # testsuite seems to fail on big endian
esac

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	cmake -B build-shared -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_SHARED_LIBS=ON \
		-DENABLE_CTEST="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build-shared

	sed -i '/add_library(glslang-default-resource-limits/ s/$/ STATIC/' StandAlone/CMakeLists.txt
	cmake -B build-static -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_SHARED_LIBS=OFF \
		-DENABLE_CTEST="$(want_check && echo ON || echo OFF)" \
		$CMAKE_CROSSOPTS
	cmake --build build-static
}

check() {
	msg "Testing shared version of glslang"
	ctest --test-dir build-shared --output-on-failure

	msg "Testing static version of glslang"
	ctest --test-dir build-static --output-on-failure
}

package() {
	# installing static first lets shared overwrite the executables so they
	# themselves are not statically linking the library
	DESTDIR="$pkgdir" cmake --install build-static
	DESTDIR="$pkgdir" cmake --install build-shared
}

sha512sums="
dc26a6b4311319e2897141bbb424eac6fc7deec99b90ac87c1f734701a8d52d55e5a6884183583f526580fe173ace01f5a5bc666658e1699a57d2e522f0edc84  glslang-1.3.261.0.tar.gz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=cantor
pkgver=23.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !s390x !riscv64 !ppc64le"
url="https://edu.kde.org/cantor/"
pkgdesc="KDE Frontend to Mathematical Software "
license="GPL-2.0-or-later"
makedepends="
	analitza-dev
	extra-cmake-modules
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	knewstuff-dev
	kparts-dev
	kpty-dev
	ktexteditor-dev
	ktextwidgets-dev
	kxmlgui-dev
	poppler-qt5-dev
	python3-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtwebengine-dev
	qt5-qtxmlpatterns-dev
	samurai
	syntax-highlighting-dev
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/education/cantor.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/cantor-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
dd3c4068691bf8706b6afdb1903b5592cd16de553be503ad11a39c8dce74c10f8be82c8195f26329996a5bc53b8df354868428b691ba82d68cca8ceb0aa015f8  cantor-23.08.0.tar.xz
"

# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdebugsettings
pkgver=23.08.0
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/utilities/"
pkgdesc="An application to enable/disable qCDebug"
license="GPL-2.0-or-later"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kitemviews-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/utilities/kdebugsettings.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdebugsettings-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
28ebe3a1dabae5445ce9d8330d2603933b53bc49159a3252d1099f36b9d03a542756416d24e583f0a9f7371ff23f3a82db3d1140dd17c6cb4ab2c5133b3b0fa6  kdebugsettings-23.08.0.tar.xz
"
